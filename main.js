targets = []
bullets = []
flaks = []
fragments = []
explosions = []
flashes = []
newBulletCounter = 0
newTargetCounter = NEW_TARGET_EVERY_MAX
n_flaks = 3

function setup(){
    createCanvas(window.innerWidth, window.innerHeight);
    for (let i = 0; i < n_flaks; i++) {
        flaks.push(new Flak(window.innerWidth * (i + 1) / (n_flaks + 1), window.innerHeight))
    }
    frameRate(FRAME_RATE)
}

function draw(){
    
    background(190, 220, 255);

    // Shoot
    if (newBulletCounter >= NEW_BULLET_EVERY && targets.length > 0) {
        for (i in flaks) {
            newBullet = flaks[i].shoot(targets)
            if (newBullet !== undefined) {
                bullets.push(newBullet)
            }
        }
        newBulletCounter = 0
    }

    // New Target
    if (newTargetCounter >= NEW_TARGET_EVERY_MIN + Math.random() * (NEW_TARGET_EVERY_MAX - NEW_TARGET_EVERY_MIN)) {
        targets.push(new Target())
        newTargetCounter = 0
    }

    
    // Handle Explosions
    for (let i = explosions.length - 1; i >= 0; i--) {
        explosions[i].update(dt)
        explosions[i].show()
        if (explosions[i].checkSettled()) {
            explosions.splice(i, 1)
        }
    }

    // Handle Targets
    targets: for (let i = targets.length - 1; i >= 0; i--) {
        if (targets[i].hitCount > 0) {
            explosions.push(new Explosion(
                targets[i].x,
                targets[i].y,
                targets[i].vx,
                targets[i].vy,
                targets[i].ax,
                targets[i].ay,
                5 + Math.random() * 10 + 20 * Math.exp(5.6 * (targets[i].hitCount/MAX_HP-1)),
                FRAME_RATE + FRAME_RATE * Math.exp(5.6 * (targets[i].hitCount/MAX_HP-1)),
                Math.min(1, targets[i].hitCount/MAX_HP)
            ))
        }
        targets[i].update(dt)
        targets[i].bulletCountResetTimer++
        if (targets[i].bulletCountResetTimer > RESET_BULLET_COUNT_EVERY) {
            targets[i].bulletCount = 0
        }
        targets[i].show()
        if (targets[i].checkOutsideBorder(window.innerWidth, window.innerHeight)) {
            targets.splice(i, 1)
            continue
        }

        // Collision 
        for (let j = bullets.length - 1; j >= 0; j--) {
            if (targets[i].checkCollision(bullets[j].x, bullets[j].y)) {
                targetDied = targets[i].collide()
                flashes.push(new Flash(bullets[j].x, bullets[j].y, targets[i].vx, targets[i].vy, targets[i].ax, targets[i].ay, 3 + Math.random() * 5, FRAME_RATE/15, 1))
                bullets.splice(j, 1)
                if (targetDied) {
                    for (let k = 0; k < Math.random() * 5; k++) {
                        fragments.push(new Fragment(targets[i]))
                    }
                    targets.splice(i, 1)
                    continue targets
                }
            }
        }
    }

    // Handle Bullets
    for (let i = bullets.length - 1; i >= 0; i--) {
        bullets[i].update(dt)
        bullets[i].show()
        if (bullets[i].checkOutsideBorder(window.innerWidth, window.innerHeight)) {
            bullets.splice(i, 1)
        }
    }

    // Handle Flashes
    for (let i = flashes.length - 1; i >= 0; i--) {
        flashes[i].update(dt)
        flashes[i].show()
        if (flashes[i].checkSettled()) {
            flashes.splice(i, 1)
        }
    }

    // Handle Fragments
    for (let i = fragments.length - 1; i >= 0; i--) {
        fragments[i].update(dt)
        explosions.push(new Explosion(fragments[i].x, fragments[i].y, fragments[i].vx, fragments[i].vy, fragments[i].ax, fragments[i].ay, 5 + Math.random() * 10, FRAME_RATE*2, 0.2))
        fragments[i].show()
        if (fragments[i].checkOutsideBorder(window.innerWidth, window.innerHeight)) {
            fragments.splice(i, 1)
        }
    }

    // Handle Flaks
    for (let i = flaks.length - 1; i >= 0; i--) {
        flaks[i].show()
    }

    newBulletCounter++
    newTargetCounter++
}