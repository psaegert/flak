class Flak {
    constructor(x, y) {
        this.x = x
        this.y = y
        this.angle = 0
        this.shot = false
    }

    aim(target){
        let currentVector = [target.x - this.x, target.y - this.y]
        let lastVector = [this.x, this.y]
    
        for (let i = 0; i < MAX_AIM_ITER && magnitude([currentVector[0] - lastVector[0], currentVector[1] - lastVector[1]]) > VECTOR_CONVERGE_THRESHOLD; i++){
            lastVector = currentVector
            let extrapolatedTargetState = target.extrapolate(magnitude(lastVector) / BULLET_SPEED)
            currentVector = [extrapolatedTargetState[0] - this.x, extrapolatedTargetState[1] - this.y]
        }
    
        return currentVector
    }

    launchAngle(x, y) {
        let angle = -Math.atan(
            BULLET_SPEED**2 / (-GRAVITY * (x - this.x))
            + Math.sqrt(
                (BULLET_SPEED**2 * (BULLET_SPEED**2 + 2 * -GRAVITY * (y - this.y))) / (GRAVITY**2 * (x - this.x)**2) - 1
            ) * ((x - this.x) < 0 ? 1 : -1)
        )
        

        if (x - this.x < 0) {
            return -Math.PI + angle
        } else {
            return angle
        }
    }

    pathTime(x, y, angle) {
        if (x == this.x) {
            return (Math.sqrt(-2 * GRAVITY * (y - this.y) + BULLET_SPEED**2) - BULLET_SPEED) / -GRAVITY
        } else {
            return (x - this.x) / (Math.cos(angle) * BULLET_SPEED)
        }
    }

    aimParabolic(target) {
        let targetPosition = [target.x, target.y]
        let currentAngle = this.launchAngle(target.x, target.y)

        let time
        let lastAngle = 0

        for (let i = 0; i < MAX_P_AIM_ITER && Math.abs(currentAngle - lastAngle) > ANGLE_CONVERGE_THRESHOLD; i++){

            lastAngle = currentAngle
            
            time = this.pathTime(targetPosition[0], targetPosition[1], currentAngle)

            targetPosition = target.extrapolate(time)

            currentAngle = this.launchAngle(targetPosition[0], targetPosition[1])
        }

        if (targetPosition[0] < 0 || targetPosition[0] > window.innerWidth) {
            return undefined
        }

        return [BULLET_SPEED * Math.cos(currentAngle), BULLET_SPEED * Math.sin(currentAngle)]
    }

    shoot(targets) {
        let closestTargetIndex = 0
        let closestTargetDistance = magnitude([window.innerWidth, window.innerHeight])
        let vectors = []
        let allTargetsServed = true
        for (let i = 0; i < targets.length; i++) {
            if (targets[i].bulletCount === 0) {
                allTargetsServed = false
            }
        }
        for (let i = 0; i < targets.length; i++) {
            vectors[i] = this.aimParabolic(targets[i])
            if (vectors[i] == undefined) {continue}
            if (targets[i].bulletCount >= MAX_BULLETS_PER_TARGET && targets.length > 1 && !allTargetsServed) {continue}
            if (vectors[i][1] > 0) {continue}

            let distanceToTarget = magnitude([targets[i].x - this.x, targets[i].y - this.y])
            if (distanceToTarget < closestTargetDistance) {
                closestTargetDistance = distanceToTarget
                closestTargetIndex = i
            }
        }

        if (vectors[closestTargetIndex] == undefined) {
            return
        }

        targets[closestTargetIndex].bulletCount++

        this.angle = Math.atan(vectors[closestTargetIndex][1] / vectors[closestTargetIndex][0])
        if (vectors[closestTargetIndex][0] < 0) {
            this.angle = -Math.PI + this.angle
        }

        this.shot = true

        return new Bullet(this.x, this.y, vectors[closestTargetIndex][0], vectors[closestTargetIndex][1], 0, -GRAVITY, BULLET_SPEED)
    }

    show() {
        fill(51)
        ellipse(this.x, this.y, 30)
        push()
        translate(this.x, this.y)
        rotate(this.angle)
        rect(0, -2.5, 25, 5)
        pop()
    }
}