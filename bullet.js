class Bullet {
    constructor(x, y, vx, vy, ax, ay, speed) {
        this.SPEED = speed
        this.x = x
        this.y = y
        this.vx = vx / Math.sqrt(vx**2 + vy**2) * this.SPEED
        this.vy = vy / Math.sqrt(vx**2 + vy**2) * this.SPEED
        this.ax = ax
        this.ay = ay
        this.lastX = x
        this.lastY = y
    }

    update(dt) {
        this.lastX = this.x
        this.lastY = this.y

        this.vx += dt * this.ax
        this.vy += dt * this.ay

        this.x += dt * this.vx
        this.y += dt * this.vy
    }

    checkOutsideBorder(width, height) {
        return this.x <= 0 || this.y <= 0 || this.x >= width || this.y >= height;
    }

    show() {
        stroke(0)
        strokeWeight(2)
        line(this.x, this.y, (this.x + this.lastX) / 2, (this.y + this.lastY) / 2)
        noStroke()
        // fill(255, 230, 100)
        // ellipse(this.x, this.y, 2)
    }
}