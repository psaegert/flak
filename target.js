class Target {
    constructor() {
        this.COLLISION_THRESHOLD = 15

        let leftToRight = Math.random() < 0.5
        this.x = leftToRight ? 0 : window.innerWidth
        this.y = window.innerHeight - (Math.random() * window.innerHeight * (1 - 0.6) + window.innerHeight * 0.6)
        this.vx = (25 + Math.random() * 50) * (leftToRight ? 1 : -1) * FRAME_RATE / 10
        this.vy = (Math.random() * 30 - 10) * FRAME_RATE / 10
        this.ax = (Math.random() * 30 - 10) * FRAME_RATE / 10
        this.ay = -GRAVITY / 2
        this.bulletCount = 0
        this.bulletCountResetTimer = 0
        this.hitCount = 0
        this.drawSize = 15
    }

    checkCollision(x, y) {
        return Math.sqrt((this.x - x)**2 + (this.y - y)**2) < this.COLLISION_THRESHOLD
    }

    update(dt) {
        let newState = this.updateStep(dt, this.x, this.y, this.vx, this.vy, this.ax, this.ay)
        this.x = newState[0]
        this.y = newState[1]
        this.vx = newState[2]
        this.vy = newState[3]
        this.ax = newState[4]
        this.ay = newState[5]

    }

    updateStep(dt, x, y, vx, vy, ax, ay) {
        vx += dt * ax
        vy += dt * ay

        x += dt * vx
        y += dt * vy

        return [x, y, vx, vy, ax, ay]
    }

    extrapolate(T) {
        let x = this.x
        let y = this.y
        let vx = this.vx
        let vy = this.vy
        let ax = this.ax
        let ay = this.ay
        let state
        for (let t = 0; t < T; t += dt) {
            state = this.updateStep(dt, x, y, vx, vy, ax, ay)
            x = state[0]
            y = state[1]
            vx = state[2]
            vy = state[3]
            ax = state[4]
            ay = state[5]
        }
        return state
    }

    checkOutsideBorder(width, height) {
        return this.x <= 0 || this.y <= 0 || this.x >= width || this.y >= height;
    }

    collide() {
        this.hitCount++
        return this.hitCount >= MAX_HP
    }


    show() {        
        noStroke()
        fill(0)
        push()
        translate(this.x, this.y)
        let directionAngle = Math.atan(this.vy/this.vx)
        if (this.vx > 0) {
            directionAngle =  -Math.PI + directionAngle
        }
        rotate(directionAngle)
        rect(-10, -3.5, 20, 7)
        ellipse(-10, 0, 7)
        pop()
    }
}

class Fragment extends Target {
    constructor(target) {
        super()
        this.x = target.x
        this.y = target.y
        this.vx = target.vx + (Math.random() * 2 - 1) * 100
        this.vy = target.vy + (Math.random() * 2 - 1) * 100
        this.ax = 0
        this.ay = -GRAVITY
        this.drawSize = 5 + Math.random() * 5
    }

    update(dt) {
        this.vx += dt * this.ax
        this.vy += dt * this.ay

        this.vx *= 0.999
        this.vy *= 0.999

        this.x += dt * this.vx
        this.y += dt * this.vy
    }

    show() {
        // noStroke()
        // fill(51)
        // ellipse(this.x, this.y, this.drawSize)
    }

}