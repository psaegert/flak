class Explosion {
    constructor(x, y, vx, vy, ax, ay, size, time, alpha) {
        this.x = x
        this.y = y
        this.vx = vx
        this.vy = vy
        this.ax = 0
        this.ay = -GRAVITY
        this.size = size
        this.time = time
        this.counter = 0
        this.alpha = alpha
    }

    update(dt) {
        this.counter++

        this.vx += dt * this.ax
        this.vy += dt * this.ay

        this.vx *= 0.93
        this.vy *= 0.95

        this.x += dt * this.vx
        this.y += dt * this.vy
    }

    r() {
        return Math.exp(- 5.6 * this.counter/this.time)
    }

    show() {
        fill(
            255 * Math.sqrt(this.r()),
            255 * this.r(),
            255 * this.r()**2,
            100 * (1 - (this.counter/this.time)**2) * this.alpha
        )
        ellipse(this.x, this.y, (1-this.r()) * this.size)
    }

    checkSettled() {
        return this.counter >= this.time
    }
}

class Flash extends Explosion{
    constructor(x, y, vx, vy, ax, ay, size, time, alpha) {
        super(x, y, vx, vy, ax, ay, size, time, alpha)
    }

    update(dt) {
        this.counter++

        this.vx += dt * this.ax
        this.vy += dt * this.ay
        
        this.x += dt * this.vx
        this.y += dt * this.vy
    }

    show() {
        fill(
            255,
            225,
            90,
            // 100 * (1 - (this.counter/this.time)**2) * this.alpha
        )
        ellipse(this.x, this.y, (1-this.r()) * this.size)
    }
}